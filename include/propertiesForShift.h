#include <BALL/KERNEL/atom.h>
#include <BALL/KERNEL/system.h>
#include <BALL/KERNEL/residue.h>
#include <BALL/MATHS/vector3.h>
#include <BALL/MOLMEC/AMBER/amber.h>
#include <BALL/DATATYPE/hashGrid.h>
#include <BALL/STRUCTURE/geometricProperties.h>
#include <BALL/STRUCTURE/defaultProcessors.h>
#include <BALL/STRUCTURE/HBondProcessor.h>
#include <BALL/STRUCTURE/rotamerLibrary.h>


#include <set>
#include <map>

namespace BALL
{

	class BALL_EXPORT BALLPropertiesForShift
	{
		public:


			BALLPropertiesForShift(System* s, std::ofstream& logstream);
			/** current target atom
			*/
			Atom* atom;

			/**	 compute for the given atom all properites specified in the property set properties
			 *
			 * 	and store them in the properties_real_ map respective the properties_string_ map  
			 **/
			bool computeProperties(PDBAtom* atom, std::set<String> properties);

			/** Method to obtain a property. 
			 * 		We return a std::pair<float, String> with the property that:
			 *    the first element is std::numeric_limits<float>::min() if it is a discrete property
			 *    the second element is the string "invalid" if it is a continuous property
			 *
			 *    NOTE: make sure, that all properties (except from CHI)  are stored either 
			 *     	in properties_real_ or in properties_string_ 
			 *     	otherwise the operator is not able to return the correct value!
			 */
			String operator [] (const String& property_name);

			static String getType(String property);
			static bool   isMixed(String property);

			static float  getChiAngle(Residue* residue);
			static float  getChi2Angle(Residue* residue);
			static char   getAminoAcid(Residue* residue);
			static char   getSecondaryStructure(Residue* residue);

			float  getHA_HBondLen(Residue* residue);
			float  getHA2_HBondLen(Residue* residue);
			float  getHN_HBondLen(Residue* residue);
			float  getO_HBondLen(Residue* residue);
			float	 getDisulphideBondLen(Residue* residue);

			float  getHA_HBondAngle(Residue* residue);
			float  getHA2_HBondAngle(Residue* residue);
			float  getHN_HBondAngle(Residue* residue);
			float  getO_HBondAngle(Residue* residue);
			float	 getDisulphideBondTorsionAngle(Residue* residue);

			float  getHA_HBondEnergy(Residue* residue); //TODO 
			float  getHA2_HBondEnergy(Residue* residue);//TODO
			float  getHN_HBondEnergy(Residue* residue); //TODO
			float  getO_HBondEnergy(Residue* residue);  //TODO
			float	 getDisulphideBondEnergy(Residue* residue);//TODO

			bool   hasDisulphideBond(Residue* residue);
			bool   hasHA_HBond(Residue* residue);
			bool   hasHA2_HBond(Residue* residue);
			bool   hasHN_HBond(Residue* residue);
			bool   hasO_HBond(Residue* residue);//TODO

			int	   getHBondIndex(Atom* atom);
			bool   hasHBond(Atom* atom);
			float  getHBondEnergy(Atom* atom); //TODO
			float  getHBondLength(Atom* atom); // distance between non-hydrogen donor and acceptor
			float  getHBondAngle(Atom* atom);

			Atom*  getNonHydrogenHBondDonor(Size pos_hbond);
			Atom*  getHBondHydrogen(Size pos_hbond);
			int    getHBondIndexForDonor(Atom* atom);
			bool   isHBondDonor(Atom* atom);
			bool   isHBondAcceptor(Atom* atom);
			bool   isHBondHydrogen(Atom* atom);

			//float  getHBondDonorLength(Atom* atom); TODO redundant to getHBondLength
			//float  getHBondDonorAngle(Atom* atom);  TODO redundant to getHBondAngle


			// Energies between the atom and all other atoms not in the residue
			// computations are made in computeProperties function to make it more time efficient
			// so 
/*			float			getAmberEnergy(Atom* atom);
			float			getAmberESEnergy(Atom* atom);
			float			getAmberVdWEnergy(Atom* atom);
			float			getAmberBondStretchEnergy(Atom* atom);
			float			getAmberAngleBendEnergy(Atom* atom);
			float			getAmberTorsionEnergy(Atom* atom);*/

			// closest rotamer int -> rmsd minimizer
			int			getNearestRotamerIndex(Residue* residue);
			bool		hasNearestRotamerIndex(Residue* residue, int index);
			float		getRotamerDistance(Residue* residue);

			// measure of atoms in the area around the atom, distance depends on the width of the hash grid(now 5 angstrom)			
			float getAtomDensity(Atom* atom, bool weighted = false); // SUM(1)
			//float			getAtomDensity(Atom* atom);	        // SUM(1)			
			//float			getAtomWeightedDensity(Atom* atom);	//SUM(1/d)

			// volume of other atoms within sphere 4 angstrom 
			float			getAtomicPacking(Atom* atom);          // 1/V*SUM(Volume of atom in Sphere)
			float			getWeightedAtomicPacking(Atom* atom);  // SUM(1/d  * Vol)


			float			getDistanceToCoM(Atom* atom);
			float			getAtomSurfaceArea(Atom* atom);
			float			getAtomSurfaceAreaSR(Atom* atom);
			float			getResidueSurfaceArea(Residue* residue);
			float			getResidueSurfaceAreaSR(Residue* residue);

			float     getDistanceToNextAtomOfType(Atom* atom, String element);

			float			getDistanceToNextNAtom(Atom* atom);  // Hashgrid the nearest Atom not in this 
			float			getDistanceToNextCAAtom(Atom* atom); // residue but might be in the next or previous
			float			getDistanceToNextCBAtom(Atom* atom); // if not in the surrounding boxes NA_float_value will be returned
			float			getDistanceToNextOAtom(Atom* atom);  //	

			float			getTemperatureFactor(PDBAtom* atom);

			Atom*     getNextHeteroAtom(Atom* atom);
			Residue*  getNextHeteroResidue(Atom* atom);
			float			getDistanceToNextHeteroAtom(Atom* atom);
			float			getDistanceToNextHeteroAtom(Atom* atom, const String& type);
			Size      getNumberOfCloseHetAtoms(Atom* atom, const String& type);
			Size      getNumberOfCloseHetAtoms(Atom* atom);
			Size      getNumberOfCloseHetElements(Atom* atom, const String& type);
			float	    getDistanceToNextHeteroElements(Atom* atom, const String& type);

			float			getHydrophobicity(Atom* atom);//TODO
			float			getSolvationFreeEnergy();     //TODO

			RotamerLibrary* getRotamerLibrary() {return rot_lib_;}


		protected:
			std::map<String, String> properties_as_strings_;
			std::map<String, float>  properties_real_;
			std::map<String, String> properties_string_;

			System*   system_;
			Vector3   CoM_;

			std::map<Chain*, Size>  protein_size_;
			Size                    ligand_size_;
			bool                    has_ligand_;
			bool                    has_dna_;
			bool                    has_ion_;

			HashMap<const Atom*, float>    atom_areas_;
			HashMap<const Atom*, float>    atom_areas_SR_;
			//HashMap<const Atom*, float>   atom_positiv_;
			//HashMap<const Atom*, float>   atom_negativ_;
			HashMap<const Residue*, float> residue_areas_;
			HashMap<const Residue*, float> residue_areas_SR_;
			//HashMap<const Residue*, float> residue_positiv_;
			//HashMap<const Residue*, float> residue_negativ_;

			HashMap<const Residue*, int>   rotamer_index_;
			HashMap<const Residue*, float> rotamer_dist_;
			RotamerLibrary*                rot_lib_;
			// to translate between rotamer_index_ and rotamer_index_per_residue_type
			HashMap<String, int>           begin_index_;

			AmberFF                        aff_;
			HashMap<const Atom*, float>    amber_energies_;
			HashMap<const Atom*, float>    amber_ES_;  //electrostatics
			HashMap<const Atom*, float>    amber_BS_;  //bond stretch
			HashMap<const Atom*, float>    amber_AB_;  //angle bend
			HashMap<const Atom*, float>    amber_VDW_; //van der waals
			HashMap<const Atom*, float>    amber_TO_;  //torsion

			HashGrid3<Atom*>    grid_;
			HashGrid3<Atom*>    ligand_grid_;
			HashGrid3<Atom*>    dna_grid_;
			//HashGrid3<Atom*>   ion_grid_;
			//HashGrid3<Atom*>   protein_grid_;

			vector<Atom*>       hetero_atoms_;

			std::ofstream&      log;

			std::vector<HBondProcessor::HBond>  h_bonds_ ;

			std::set<String>    gaff_typenames_;
	};

}
