#!/bin/bash

#
#		NightShift: NMR shift inference by General Hybrid model Training 
#		 --- A pipeline for automated NMR shift prediction training ---
#		
#		Anna Dehof
#

## flag for single or multiple user setting (used for directories data, results and log files)
single_user_mode=true  #TODO unused!    
debug=false

## define the source_dir 
export source_dir=$(pwd)
export PATH=${source_dir}:$PATH

## put the path to the directory where the results will be stored
## please note that the results will become pretty large (several Gb)
export datadir="@NIGHTSHIFT_OUTPUT_DIRECTORY@/"
export resultsdir="${datadir}results/"
export logdir="${datadir}/logs/"

## path to the ball installation
export ball_installation_directory="@BALL_INSTALL_DIRECTORY@"
export BALL_DATA_PATH="@BALL_DATA_PATH@"
export BALL_PYTHONPATH="@BALL_PYTHONPATH@"
export PYTHONPATH=$PYTHONPATH:@BALL_PYTHONPATH@:@BALL_CONTRIB_PATH@/lib:@SIP_MODULE_PATH@

## path to the pisces binary, if available, or empty
export pisces_maxpc="10"
export pisces_parameters="-r 0.0-3.0 -l 40-10000 -f 1 -x T -a T"
export pisces_path="@PISCES_PATH@"
export pisces_binary="@PISCES_EXECUTABLE@"

## path to the ShiftX2 installation, if available, or empty
#export shiftx2_path=""
export shiftx2_path="@SHIFTX2_PATH@"
export shiftx2_executable="@SHIFTX2_EXECUTABLE@"
export shiftx2_results_path="@SHIFTX2_RESULTS_PATH@"

## path to the clustalw binary
export clustalw_binary="@CLUSTALW_EXECUTABLE@"

#export prediction_type=#"PureProtein" # can be PureProtein or ProteinLigand
export prediction_type="PureProtein" # can be PureProtein or ProteinLigand

export mappingfilename_protein="$datadir/mapping_PROTEIN.txt"       # protein
export mappingfilename_ligand="$datadir/mapping_LIGAND.txt"         # protein-ligand
export mappingfilename_dna="$datadir/mapping_DNA.txt"               # dna
export mappingfilename_dna_ligand="$datadir/mapping_DNA_LIGAND.txt" # dna-ligand
export mappingfilename=${mappingfilename_protein}   

export feature_file_protein="@CMAKE_INSTALL_PREFIX@/data/features.txt"
export feature_file_ligand="@CMAKE_INSTALL_PREFIX@/data/features_ligand.txt"
export feature_sig_cutoff=0.5
export feature_file=${feature_file_protein}

export pdbhome="$datadir/PDB_PROTEIN/"                                                     # protein
export pdbhome_ligand="$datadir/PDB_LIGAND/"                                               # protein-ligand
export pdbhome_dna="$datadir/PDB_DNA/"                                                     # dna
export pdbhome_dna_ligand="$datadir/PDB_DNA_LIGAND/"                                       # dna-ligand
export pdbhome_shiftx2_set="$datadir/shiftx2-testset-June2011/PDB-testset-addHydrogens/"   # ShiftX2 test set #TODO delete ?

export bmrbhome="$datadir/BMRB_PROTEIN/"                # protein
export bmrbhome_ligand="$datadir/BMRB_LIGAND/"          # ligand
export bmrbhome_dna="$datadir/BMRB_DNA/"                # dna
export bmrbhome_dna_ligand="$datadir/BMRB_DNA_LIGAND/"  # dna-ligand
export bmrbhome_refdb="$datadir/BMRB_REFDB/"            # reference corrected

export nmr_sqlite_db="$datadir/nmr_shift.db"
export nmr_sqlite_db_ligand="$datadir/nmr_shift_ligand.db"

export num_filldb_procs=4                               # number of parallel processes to compute atom properties


