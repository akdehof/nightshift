###
###		NightShift: NMR shift inference by General Hybrid model Training 
###		 --- A pipeline for automated NMR shift prediction training ---
###		
###		Anna Dehof
###
###   This file defines the features to compute for 
###   the NightShift pipeline.
###   These features aim for general analysis, training, and 
###   prediction from an existing model.
### 
###   The format is as follows:
### 
###      key 	: description
###
##############################################################
##############################################################
###
###
###    The following default features are automatically 
###    computed, e.g. for correct database handling...
###
### EXP_SH		: experimental shift as denoted in the BMRB file  
### COMP_SH		: sum of all semi classical contributions including EHS
### ATOM_NAME	: atom name according to Amber nomenclature
### PDB_ID		: PDB identifier
### CHAIN_ID	: chain identifier
### RES_ID		: residue identifier
###
##############################################################
###
### 	semi-classical chemical shift contributions
###
RING_CURR 	: ring current 
ELEC_FIELD	: electric field
RANDOM_COIL	: random coil 
HBOND_EFF		: hbond effect
#EHS				: empirical hyper surface, EHS
###
###     ##	 other features   ##
### 
ELEMENT	: atomic element
FR			: flag indicating N-terminal	
AA		: amino acid type
SS		: secondary structure type 
PSI		: Psi angle
PHI		: Phi angle
CHI		: Chi angle
CHI2	: Chi2 angle 
###
AAnext		: next amino acid type	
PSInext		: next Psi angle
PHInext		: next Phi angle
CHInext		: next Chi angle
CHI2next	: next Chi2 angle
###
AAprev		: previous amino acid	type
PSIprev		: previous Psi angle
PHIprev		: previous Phi angle
CHIprev		: previous Chi angle
CHI2prev	: previous Chi2 angle
###
HA		: flag for residue's HA is involved in hbond
HA2		: flag for residue's HA2 is involved in hbond
HN		: flag for residue's HN is involved in hbond
OH		: flag for residue's OH is involved in hbond
###
HAL		: HA1 hbond length
HA2L	: HA2 hbond length
HNL		: HN  hbond length
OHL		: O   hbond length
HAA		: HA1 hbond angle
HA2A	: HA2 hbond angle
HNA		: HN  hbond angle
OHA		: O   hbond angle
###
HBONDHYDROGEN : flag for involvement of the current atom as hbond hydrogen #TODO 
HBONDACCEPTOR	: flag for involvement of the current atom as hbond acceptor 
HBONDACCEPTORA	: its hbond angle 
HBONDACCEPTORL	: its hbond length
###
HBONDDONOR	: flag for involvement of the current atom as hbond donor 
#HBONDDONORA	: its hbond angle
#HBONDDONORL	: its hbond length
###
DISULPHIDE		: flag for the residue's involvement in a disulphide bond
DISULPHIDEL		: disulphide bond length	
DISULPHIDETA	: disulphide torsion angle
###
CHARGE		: atom partial charge
COM				: distance to center of mass
ATOMSA		: surface area of atom with 1.5\AA probe
ATOMSASR	: surface area of atom with 0.5\AA probe
RESIDUESA	: surface area of residue with 1.5\AA probe
RESIDUESASR	: surface area of residue with 0.5\AA probe
###
AMBERALL	: Amber energy and single contributions
AMBERES		: Amber's electrostatic energy contribution 
AMBERVDW	: Amber's van-der-Waals energy contribution
AMBERSTRETCH	: Amber's bond stretch energy contribution
AMBERBEND			: Amber's bend energy contribution
AMBERTORSION	: torsional Amber energy contribution
###
ATODEN		: atom density within 5\AA radius
ATOWEIDEN	: weighted atom density within 5\AA radius
ATOPAC		: atom packing within 5\AA radius
ATOWEIPAC	: weighted atom packing within 5\AA radius
###
DISTCA		: distance to the closest CA atom, excluding the atom's own residue
DISTCB		: distance to the closest CB atom, excluding the atom's own residue
DISTN			: distance to the closest N  atom, excluding the atom's own residue
DISTO			: distance to the closest O  atom, excluding the atom's own residue
###
ROTAMER			: index of rotamer with smallest RMSD 
ROTAMERDIST	: RMS distance to the most similar rotamer type of the atom's residue
###
###  the rotamers 
###  Due to its typical size set, we split the rotamers based on the amino acid type.
###  The keyword "ROTAMERINDEX_" will be checked in the method ComputeNMRDescriptors.C 
###  to compute the number of variables needed during runtime, 
###  e.g., the Dunbrack rotamer library yields ROTAMERINDEX_ASN_1 and ROTAMERINDEX_ASN_2.
###
ROTAMERINDEX_	: most similar rotamer state of the atom's residue
#
######################################################################
######################################################################
###
###    ## mapping features ##
###    ## and general NMR information ###
### 
#### TODO: test!!!
#ALIGN_SCORE		: score of the alignment
#Alignment_PDB	: gapped alignment from PDB
#Alignment_BMRB	: gapped alignment from BMRB
#PDB_R_FACT			: PDB's R factor
#TempFac		    : TODO currently not implemented 
#PDB_YEAR				: year of the PDB experiment
#NMR_YEAR				: year of the NMR experiment
PROTEIN_SIZE	: number of protein atoms
#HAS_H_SHIFTS	: flag indicating availability of H shifts
#HAS_C_SHIFTS	: flag indicating availability of C shifts
#HAS_N_SHIFTS :	flag indicating availability of N shifts
#IS_NMR 			: flag indicating if PDB file was deduced from NMR experiment 
#NMR_SPECT 		: spectrometer as stored in the NMR file
#NMR_SPECT_MANUFAC	: manufacturer of the NMR spectrometer
#NMR_FIELD_STRENGTH	: field strength applied during the experiment
#HAS_ION		: indicator for presence of ion\s in the PDB file
#HAS_LIGAND	: indicator for presence of ligand\s in the PDB file
#HAS_DNA		: indicator for presence of DNA in the PDB file
LIGAND_SIZE	: number of ligand atoms
#IONS				: 	
#NMR_SOLUTION	: name of the solution stored in the NMRStar file 
#PH 			: pH value stored in the NMRStar file  
#TEMP 		: temperature value stored in the NMRStar file 
#PRESS 		: pressure value stored in the NMRStar file 
# 
######################################################################
######################################################################
###
###  this part targets at __LIGANDS__ close to protein atoms
###
### 
###  
###     ##   BMRB/ NMR   ##
###
#HAS_ION		: indicator for presence of ion\s in the PDB file
#HAS_LIGAND	: indicator for presence of ligand\s in the PDB file
#HAS_DNA		: indicator for presence of DNA in the PDB file
###
###     ##	 other features   ##
### 
#LIGAND_SIZE		: number of ligand atoms
#NUM_HET_ATOMS	: number of ligand atoms within 10\AA distance 
#CL_HET_ATOM		: atom element type of closest ligand hetero atom
#CL_HET_GAFFType	: GAFFtype of the closest ligand hetero atom
#CL_HET_DIST		: the distance to the closest ligand hetero atom
###
### GAFF Types, due to the large number of possible GAFF Types
### we use the same key-word mechanism as for the rotamers
###
#GAFFTYPE_			:	indicator for the presence of GAFF type X within 10\AA radius		
#GAFFTYPE_DIST_	:	distance to the closest ligand atom of GAFF type X within 10\AA radius	
#HET_ELEMENT_		: indicator for the presence of a ligand atom of element type X
#HET_ELEMENT_DIST_	: distance to the closest ligand atom of element type X
#
