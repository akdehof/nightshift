#!/bin/bash -f

## first argument:  the db to merge into
## second argument: the db to merge from
## third argument:  the table to merge

cmd="attach '$2' as to_merge; begin; insert into $3 select * from to_merge.$3; commit; detach database to_merge;"
echo $cmd | sqlite3 $1
