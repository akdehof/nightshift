#!/usr/bin/python
import os
import sys 

#adapted_features_filename = "$$_features.txt"
feature_mask_filename    = "../galaxy-tools/FeatureGenerator-galaxy.xml"

feature_header = "<!--This is a configuration file for the integration of a BALLSuite tool into Galaxy (http://usegalaxy.org). This file was automatically generated using ballaxy, so do not bother to make too many manual modifications.-->\n<tool id=\"featuregenerator\" name=\"FeatureGenerator\" version=\"0.9.6\" force_history_refresh=\"True\">\n\t\t<description>collects all features for the training of an NMR shift prediction model</description>\n\t\t<command interpreter=\"bash\">\n\t\t /home/HPL/anne/DEVELOP/GALAXY/NMR/echoHelper.sh -e \" \n"

feature_xml_parser_line="#if str( $%s ) != ''  and str( $%s ) != 'None' :\n\t\"$%s\" : %s \\n \n#end if\n" # %(feature_string, feature_string, feature_string, description)

feature_bridge = " \" > $outfile\n\t\t</command>\n\t\t<inputs>\n " #%(adapted_features_filename)

feature_xml_parameter_line = "\t\t\t<param name=\"%s\" type=\"boolean\" checked=\"True\" label=\"%s\" truevalue=\"%s\" falsevalue=\"# %s\"/>\n" # %(feature_string, description, feature_string, feature_string)

feature_footer="\t\t</inputs>\n\t\t<outputs>\n\t\t\t<data name=\"outfile\" format=\"txt\"/>\n\t\t</outputs>\n\t\t<help>This tool creates a feature specification file.\n\nBased on the given feature specification file, already implemented features can be activated or deactivated.\n\nTo add a new feature, one has to add its implementation in the BALL tools and add a corresponding string to the feature file.\n\nOutput of this tool is feature specification file.\n\t\t</help>\n</tool>"

#########################################################################


if (len(sys.argv) < 2):
  print "Usage: %s general_feature_file" % sys.argv[0]
  print "Usage: the program automatically generates an xml mask for generating a user specified feature file." #, adapted_features_filename
  exit (1)

outfile = open(feature_mask_filename, 'w')

###
###  read the features 
###

# for debugging
#feature_string = "EXP_SH"
#description   = "experimental shift as denoted in the BMRB file"

# the feature set
features = []

lines = open(sys.argv[1], 'r').readlines()
for line in lines: # [:26]:
  if (line[0] != "#"):
    tmp = line.split(":")
    features.append((tmp[0].strip(), tmp[1].strip()))

#print features

###
###  write the mask
###


# the header
outfile.write(feature_header)

for feature in features: #.iteritems():
  outfile.write(feature_xml_parser_line %(feature[0], feature[0], feature[0], feature[1]))
  #print (feature_xml_parser_line %(feature_string, feature_string, feature_string, feature_string))

# the bridge
outfile.write(feature_bridge)

# the parameters
for feature in features:#.iteritems():
  outfile.write(feature_xml_parameter_line %(feature[0], feature[1], feature[0], feature[0]))

# the footer
outfile.write(feature_footer)

#done
outfile.close() 
print "Done."
