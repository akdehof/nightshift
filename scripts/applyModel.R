#!/usr/bin/Rscript

#
#   NightShift: NMR shift inference by General Hybrid model Training 
#	   --- A pipeline for automated NMR shift prediction training ---
#
#   Anna Dehof

#   This program applies a shift model to a protein given the model (*.Rdata) 
#   and a sqlite db containing the proteins features (*.db).

library(RSQLite)
library(MASS)
library(randomForest)

# if you want to prevent that we use command line arguments,
# just run
#   nmr_noinit <- 1
# before sourcing
if (!exists("nmr_noinit")) {
	args <- commandArgs(TRUE)
} else { 	
		# TODO check if correct! 
     args <- c("-query_db=2JY8.db", "-model_name=/home/HPL/anne/Desktop/Protein_RandomForestModel.Rdata", "-outfile=Spinster.csv")
}

source("NMR_functions.R")	 

############## TODO use -log_file as option!


## check the parameters
if (length(args)==0){
  # TODO: decide what is optional and what not!
  # TODO: bleibt Format bei tabular? 
  # TODO: generalize for Liops! if  train_ligand ... 

	# give a usage hint
	# ./applyModel.R -query_db=../build/test.sqlite -model_name=Protein_all_RandomForestModel.Rdata -outfile=test.shift
	print("This tool evaluates a given sqlite file containing NMR descriptors of a PDB file on an R model to predict NMR chemical shifts.\n\nParameters are an R-dataframe ('-model_name') and the PDB's descriptor file (-query_db).")
	print("Output of this tool is a csv file with the NMR chemical shift prediction in tabular format.")

  #exit 
  stop("Missing parameters") # TODO: This __halts__ the execution: do we want this?

} else {
  
  # parse the arguments
  # read command line args and convert in variables
  setDefaultPipelineArgs()
  handleCommandArgs(args)

  #TODO: check whether all required command line arguments are given (!= "")
  if (query_db == ""){
    print("No query sqlite db given - abort.")
    ## TODO abort, aber wie
    stop("No query sqlite db given - abort.") #return()# TODO: This __halts__ the execution: do we want this?
  }
  if (model_name == ""){
    print("No model given - abort.")
    ## TODO abort, aber wie?# TODO: This __halts__ the execution: do we want this?
    stop("No model given - abort.") #return()
  }
  if (outfile == ""){
    if (train_ligand){
      outfile = "Liops"
    } else {
      outfile = "Spinster"
    }
    print(paste("No outfile given - use default ", outfile, ".csv ."))
  }
}

#######################################

print(paste("** Loading the query database ", query_db))

# connect to tht DB
connectDB(query_db)

print(paste("** Loading the prediction model ", model_name))
load(model_name)

verbose <- TRUE

# create the atom super classes
createAtomTypes() 

total_shifts <- data.frame()

# the main loop
for (element_type in names(atom_superclasses)){ #[[1]]) { 
		print("****************************************************")
		print(paste("** Evaluate the protein model for atom type... ", element_type, separator=""))
		
		# join tables ATOM_PROBS and PDB_BMRB
		data_shifts <- getShiftDataForEvaluation(element_type, atom_superclasses, 1e20)
		print(paste("** Data size:", dim(data_shifts)))
		
		#find out if columns are missing!
		missing_columns <- which(!names(models[[element_type]]$forest$xlevels) %in% colnames(data_shifts))
		if (length(missing_columns)>0)
		{
			print(paste(" ERROR: Missing columns in query db: ", names(models[[element_type]]$forest$xlevels[missing_columns])))
			stop("")
		}

		data_shifts <- fixTerminalResidues(data_shifts)
		data_shifts[data_shifts$AAprev=="?", "AAprev"] <- "TER" 
		data_shifts[data_shifts$AAnext=="?", "AAnext"] <- "TER"
		data_shifts <- forceColumnTypes(data_shifts, models[[element_type]], column_types[[element_type]], TRUE)
		data_shifts <- fixTerminalResidues(data_shifts) # TODO: once should suffice...
		data_shifts <- fixBondLengths(data_shifts)
		
		# restrict to the columns used in the model
		model_selector <- colnames(data_shifts) %in% names(models[[element_type]]$forest$xlevels)
		
	
		print("** The size of the raw data set:")
		print(dim(data_shifts[,model_selector]))

		print("** Predict the single protein contribution...")
		temp_res <- predict(models[[element_type]], data_shifts[,model_selector]) #+ data_shifts[,"RANDOM_COIL"]
	
		# store the result in new column
		data_shifts["BALL_PROT_SH"] <- round(temp_res + as.real(data_shifts[,"RANDOM_COIL"]),4)
    
    # collect the predictions 
		total_shifts <- rbind(total_shifts, data_shifts)
}
pdb_id <-(data_shifts[1,"PDB_ID"])
#print(pdb_id)

# prepare the output
shift_file <- total_shifts[, c("RES_ID", "AA", "ATOM_NAME", "BALL_PROT_SH")]
# reorder
shift_order <- order(shift_file[,1])

## print all shifts
## emulate the csv format:
#NUM,RES,ATOMNAME,SHIFT
#98,E,C,175.9718
#98,E,CA,55.8715
#98,E,CB,30.6982

# TODO: print a nicer heading

# and write	
write.table(shift_file[shift_order,], file=outfile, quote=FALSE, sep=",", row.names=FALSE)

# done.
print("Done.")
