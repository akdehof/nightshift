#!/usr/bin/Rscript

#
#		NightShift: NMR shift inference by General Hybrid model Training 
#		 --- A pipeline for automated NMR shift prediction training ---
#		
#		Anna Dehof
#

library(RSQLite)
library(MASS)
library(outliers)
library(randomForest)
library(e1071)
library(foreach)

# if you want to prevent that we use command line arguments,
# just run
#   nmr_noinit <- 1
# before sourcing
if (!exists("nmr_noinit")) {
	args <- commandArgs(TRUE)
} else { 	
		# TODO check if correct! 
  	args <- c("-training_db=/local/anne/NMR/pipeline/nmr-data/nmr_shift.db", "-use_random_forest=TRUE", "-train_ligand=FALSE", "-model_name=Protein_RF_model.Rdata", "-feature_file=features_sig.txt")#, "-train_size=") 
}

verbose<-TRUE

source("NMR_functions.R")	
setDefaultPipelineArgs()
handleCommandArgs(args)

if (!train_ligand) {
	train_percentage  = 0.6 
} else {
	train_percentage  = 0.70
}

if (exists("train_size")) {
	train_percentage = as.real(train_size)
}

paste(" ** Options: ** ")
paste(" ** option prediction_column = ", prediction_column, sep="")
paste(" ** option validation_column = ", validation_column, sep="")
paste(" ** option train_percentage  = ", train_percentage,  sep="")
paste(" ** option outlier_cutoff    = ", outlier_cutoff,    sep="")
paste(" ** option min_align_score   = ", min_align_score,   sep="")
paste(" ** option max_amber_energy  = ", amber_cutoff,      sep="")
#paste("   = ",    ,    sep="")
 
## this call generates the atom_superclasses
createAtomTypes()
if (verbose) {
  print(paste("    Number of models to train:", length(names(atom_superclasses))))
}

# connect to the DB
connectDB(training_db)

####################################### 
#
#               main 
#
#######################################

#train and evaluate a model

# for a ligand model, we need to load the protein models first
if (train_ligand) {
	print (paste("** Loading protein model", model_name))
	load(model_name)
}

# generate the hash maps into which we can store the results
createResultVectors()

## the main loop
for (element_type in names(atom_superclasses)){ #[[1]]) { 
		print("*********************")
		if (!train_ligand) {
			print(paste ("** building protein model for ", element_type, separator=""))
		} else {
			print(paste ("** building ligand model for ", element_type, separator=""))
		}
		print("*********************")
		
		## join tables ATOM_PROPS and PDB_BMRB
		data_shifts <- getShiftData(element_type, atom_superclasses, min_align_score, amber_cutoff)

		data_shifts <- data_shifts[complete.cases(data_shifts[[prediction_column]]),]
		print(paste("initial data size", dim(data_shifts)))

		## add flag for terminal residues
		data_shifts <- fixTerminalResidues(data_shifts)
		data_shifts <- fixBondLengths(data_shifts)

		## TEST: remove the worst outliers
		data_shifts <- removeOutliersByScore(data_shifts, outlier_cutoff)
		
#if (!train_ligand) {
#			# pure protein case
#			# exclude columns with NA's
#			data_shifts <- excludeNAValues(data_shifts, FALSE)
#		}
#		data_shifts <- fixColumnTypes(data_shifts)

		## required for the ligand only, but must be defined
		## outside of the "if"
		shift_data_orig <- data.frame()

		## if single PROTEIN
		if (!train_ligand) {
			# substract the random coil 
			data_shifts <- subtractRC(data_shifts)
			# TODO !!!  can ge ignore this??   
			data_shifts <- prepareShiftData(element_type, data_shifts, FALSE)
		} else {## if LIGAND	
			## LIGAND only: force the column types of the protein model features to the
			##              same values as in the protein case
			## NOTE: it seems as we dont need this any longer! the types work as is
#			data_shifts <- forceColumnTypes(data_shifts, models[[element_type]], 
#					                            column_types[[element_type]], use_random_forest)

      ## In general, we don't need the outlier removal. But in the ligand data set,
			## there are several N atoms that are clearly mis-assigned (the shifts are wrong by
			## a value of about 100). These can be avoided by using a very mild outlier removal
			## in this case
			if (element_type == "N") {
			 data_shifts <- removeOutliersByScore(data_shifts, outlier_cutoff)
			}
			## LIGAND only: only retain atoms (lines) that are close to at least one GAFF atom
			data_shifts  <- fixupDistColumns(data_shifts)

			gaff_indices <- grep("GAFFTYPE_[0-9]", colnames(data_shifts))	
			data_shifts  <- data_shifts[apply(data_shifts, 1, function(x) sum(as.integer(x[gaff_indices]))>0),]

			## LIGAND only: subtract the predicted shifts
			data_shifts     <- createResidualShift(data_shifts, models)
			shift_data_orig <- data_shifts
			
			data_shifts     <- fixColumnTypes(data_shifts)
			
			prediction_column = "RESIDUAL_SH"

			data_shifts <- data_shifts[complete.cases(data_shifts[[prediction_column]]),]
		}

		
		##############################################
		## 
		##	build a selector
		##
		########### 
		
		## exclude certain columns from training 
    if (exists("feature_file")) {
	    selector <- buildSelectorByFeatureFile(data_shifts, feature_file, env = .GlobalEnv)
    } else {
      selector <- buildSelector(data_shifts, env = .GlobalEnv)
    }
		
    ## in case some added the EXP_SH to the feature list 
		if (prediction_column=="EXP_SH")
		{
			data_shifts$EHS<-NULL	
		}

		splitDataSet(train_indices, test_indices, data_shifts, element_type)
		
		print("The final training data set:")
		print(dim(data_shifts[train_indices, selector]))
		print(str(data_shifts[train_indices, selector]))	
		print("The final columns:")
		print(colnames(data_shifts[train_indices, selector]))
	

 		################################################################
		#
		#  train
		#
		###############
		if (!train_ligand) {
				indices_train[[element_type]] <- train_indices
				indices_test[[element_type]]  <- test_indices
		} else {
				ligand_indices_train[[element_type]] <- train_indices
				ligand_indices_test[[element_type]]  <- test_indices
		}

		if (!use_random_forest)
		{
			if (!train_ligand) {
				models[[element_type]] <- createLinearModel(data_shifts, train_indices, selector)
			}
			else {
				ligand_models[[element_type]] <- createLinearModel(data_shifts, train_indices, selector)	
			}
			title_prefix = paste("LinearModel")
		}
		else # if random forest
		{	
			best_mtry <- -1
			# tune
			if (train_ligand) {
				print("plot the features")
				plotFeatureVsResponse(data_shifts, selector, "RESIDUAL_SH", paste("features_", element_type, ".pdf", sep=""))

				best_mtry <- tuneRandomForest(data_shifts, train_indices, selector, element_type, train_ligand)
			}

			#for( i in colnames(data_shifts)){print(paste(i, class(data_shifts[[i]])))}
			if (!train_ligand) {
				models[[element_type]] <- tryCatch(createRandomForest(data_shifts, train_indices, selector, best_mtry, 50, 100, 500))
			} else {
			#######			data_shifts <- fixColumnTypes(data_shifts) # this destroyed the selector!!!
				ligand_models[[element_type]] <- tryCatch(createRandomForest(data_shifts, train_indices, selector, best_mtry, 5, 100, 500))
			}
			title_prefix = paste("RandomForestModel")
		}	
	
		if (!train_ligand) {
			# store column types	
			column_types[[element_type]] <- sapply(data_shifts[,selector], class)
			#for debug: 
			#models[[element_type]]$forest$xlevels	
			#print("     column:types : ")
			#print(column_types[[element_type]])
		}

		#######################################
		#
		#  evaluate 
		#
		###############
		prediction_column = "EXP_SH"
		validation_column = "COMP_SH"

		if (use_ref_db) {
			prediction_column = "RefDB"
		}

	  	
		if (train_ligand) {
			title_prefix = paste(title_prefix, "_ligand", sep="")
		}

		if (!train_ligand) {
			test_res  <- evaluateModel(models[[element_type]], data_shifts, train_indices, test_indices, element_type, selector)
		} else {
			# LIGAND only: add the ligand model to the protein response
#			data_shifts["COMP_SH"] <- data_shifts["COMP_SH"] + 	data_shifts["PROT_SH"] 
			test_res  <- evaluateLigandModel(ligand_models[[element_type]], data_shifts, train_indices, test_indices, element_type, selector, shift_data_orig)
		}

    ## plot result	
		if (validation_column %in% colnames(data_shifts)) {
			tryCatch(plotModel(title_prefix, data_shifts, element_type, test_indices, prediction_column, validation_column))
		}

		if (use_random_forest){
			tryCatch(plotVarImpPlot(title_prefix, element_type, models[[element_type]], data_shifts$prediction_column))
		}
		
## store the model
#		if (!train_ligand) {
#			saveModel(title_prefix, data_shifts, element_type, models[[element_type]], train_indices, test_indices)
#		} else {
#			saveModel(title_prefix, data_shifts, element_type, ligand_models[[element_type]], train_indices, test_indices)
#		}

		#save.image( file=filename)
		#load('LinearModel_N.Rdata')
	}

# Store the indices of the training- and test data set
if (test_training_file != "") {
	data_set_indices <- t(rbind(all_indices, all_ds_type, all_element_type))
	colnames(data_set_indices) <- c("rowid", "type", "model")
	write.csv(data_set_indices, file=test_training_file, quote=FALSE, row.names=FALSE)
}

print(paste("The final model will be stored as",model_name))

# store the overall data
if (!train_ligand) {
	storeAllModel(con, selector, indices_train,	indices_test, models, column_types, model_name) #paste("Protein_all_", title_prefix, ".Rdata", sep=""))
} else {
	storeAllModel(con, selector, ligand_indices_train,	ligand_indices_test, ligand_models, column_types, model_name) #paste(title_prefix, "_all", ".Rdata", sep=""))
}

print("Done.")
