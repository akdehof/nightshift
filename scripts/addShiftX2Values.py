#!/usr/bin/python

import sys, os, sqlite3
import urllib2, urllib, xml.dom.minidom
import re, shutil, pprint
import BALL

from nmr_definitions import *

sys.path.append(shiftx2_path)

class Options:
  atoms = "ALL"
  ph = 5.0
  temp = 298.0

import shiftx2_util


## for debugging
first_id = "1UGT"
perform = True
is_shiftX2 = False
## end debugging


####
def createShiftX2OutfileName(pdb_id, chain_id, suffix):
  outfile = shiftx2_result + "/" + pdb_id + "_" + chain_id + suffix 
  return outfile

#####
def run_shiftx2(pdb_home, shiftx2_home, pdb_id, chain_id, suffix, command_string):
  infile  = pdb_home + "/" + pdb_id + ".pdb"
  outfile = createShiftX2OutfileName(pdb_id,  chain_id, suffix)
  #outfile = shiftx2_result + "/" + pdb_id + ".pdb.shiftx2" 
  #print "shiftX2 infile: ", infile 
  #print "shiftX2 outfile: ", outfile

  #if (is_shiftX2):
  #  pattern = pdb_home+"*_"+pdb_id+chain_id+".pdb"
  #  #print pattern
  #  infile = os.popen("ls " + pattern).read().strip()

  # check if previously computed 
  if (not os.path.exists(outfile)):
    print "Call ShiftX2..."
    try:
      cmd =  (command_string) % (shiftx2_path, infile, chain_id, outfile)
      print cmd
      os.system(cmd) 
      print "done"
    except:
      print "ShiftX2 error for ", pdb_id
      return False
  else:
    print "ShiftX2 already computed for", pdb_id
  return True



#####
def addColumn(colname, conn):
  try:
    conn.execute(("alter table ATOM_PROPS add column %s REAL default -1.0" % colname))
  except:
    print ("%s column already exist" % colname)

  return  


#####
def addValues(result_file, pdb_id, chain_id, colname):
  print "Add values for **", colname, "** from file", result_file, "... "
  converter = BALL.Peptides.NameConverter()
  # add shiftx2
  if (os.path.exists(result_file)):
    # parse the output
    results = open(result_file).readlines()
    print len(results), "shift values found..." 
    fail_counter = 0
    for r in results:
      s_sp = r.strip().split(",")
      if (len(s_sp)>2):
        residue_index = s_sp[0]
        residue_name  = s_sp[1]
        atom_name     = s_sp[2]
        value         = s_sp[3]
        ball_name = converter.convertName(residue_name, atom_name, "BMRB", "PDB")
        #print residue_index, residue_name, ball_name, atom_name,  value, ball_name
        #TODO: handle different naming conventions!
        try:
          update_cmd = "update ATOM_PROPS set %s=\'%s\' where PDB_ID=\'%s\' and CHAIN_ID=\'%s\' and ATOM_NAME=\'%s\' and RES_ID=\'%s\'" % (colname, value, pdb_id, chain_id, ball_name, residue_index)
          c2.execute(update_cmd)
          #print update_cmd
          #print "." ,
          #if c2.fetchone()== None:
          #  print "Error for atom",  pdb_id, chain_id, residue_index, ball_name, value
        except:
          fail_counter += 1
          #print "*", 
          #print "no update for atom ", atom_name
      else:
        print "Error in line ", r
  else:
    print "WARNING: Missing results file", result_file
  print "Adding failed for", fail_counter, "out of", len(results), "atoms"


#####################################
#
#          main
#
#####################################

if (len(sys.argv) < 2):
  print "Usage batch mode: %s  --DB=... --PDBHOME=... --SHIFTX2HOME=..."  % sys.argv[0]  
  print "Usage explicit mode: %s  --DB=... --PDBID=... --SHIFTX2HOME=..."  % sys.argv[0]
  exit (1)

batch_mode=True

####  parse the command line arguments #####
command_args = {}
for arg in sys.argv[1:]:
  sp = arg.split("=")
  command_args[sp[0][2:]] = sp[1]

## for debugging
#import pprint
#pprint.pprint(command_args)

if not command_args.has_key("DB"):
  print("Error: No database given!")
  exit (1)

nmr_sqlite_db = command_args["DB"]

pdb_home = ""
if (command_args.has_key("PDBHOME")):
   pdb_home = command_args["PDBHOME"] 

shiftx2_home = ""
if (command_args.has_key("SHIFTX2HOME")):
   shiftx2_home = command_args["SHIFTX2HOME"] 

pdb_id = ""
#pdb_home
if (command_args.has_key("PDBID")):
  pdb_id = command_args["PDBID"] 
  batch_mode=False
  # full path or single id?  
  # TODO: pdb_id = os.path.basename(pdb_id)
  v = pdb_id.strip().split("/")
  if len(v) > 1:
    pdb_home = pdb_id[:-(len(v[-1]))]
    pdb_id = v[-1]
  if (pdb_id[-4:] == ".pdb"):
    pdb_id = pdb_id[:-4]
  if (not os.path.exists(pdb_home + "/" + pdb_id + ".pdb")): 
    print "Error: Invalid PDB given! ", pdb_home + "/" + pdb_id + ".pdb"
    exit (1)

  print "Set pdb id: ", pdb_id
print "Set pdb_home: " , pdb_home 

shiftx2_result = ""
if (command_args.has_key("SHIFTX2RESULTS")):
  shiftx2_result = command_args["SHIFTX2RESULTS"]
  if (not os.path.exists(shiftx2_result)):
    try:
      os.system("mkdir " + shiftx2_result)
    except:
      print "Could not create directory ", shiftx2_result
      exit (1)

    print " Created SHIFTX2 directory ", shiftx2_result
else:
  print("Error: No shiftX2 results dir given!")
  exit (1)


print "Assume shiftx2 path: ", shiftx2_path

#### END OF PARSING ####

# connect to the db
print "Connecting to DB..." + nmr_sqlite_db
conn = sqlite3.connect(nmr_sqlite_db)

# and add a column for the ShiftX2 values
print "Add ShiftX2 columns..."
addColumn("ShiftX2", conn)
addColumn("ShiftX1", conn)
addColumn("ShiftXP", conn)
addColumn("ShiftXN", conn)

# and read the mapping table
print "Read the mapping table..."
c  = conn.cursor()
c2 = conn.cursor()
my_query = "select PDB_ID, PDB_CHAIN_ID, PDB_CHAIN_INDEX from PDB_BMRB"
if batch_mode:
  my_query += " where NON_REDUNDANT_10 = 1" 
else: # not batch_mode:
  # TODO vllt auch die Chain mit uebergeben?
  my_query += " where PDB_ID ='" + pdb_id + "'" 

c.execute(my_query)

for row in c:
  # run shiftx 2
  pdb_id = row[0]
  print "-----------------------------------"
  print "     PDBID: ", pdb_id, 
  if (pdb_id == first_id):
    perform = True
  chain_id = row[1]
  print "ChainID: ", chain_id
  chain_index = row[2]
  if (perform and chain_id != ""):
    #full_pdb_path = pdb_home + pdb_id 
    #def run_shiftx2(pdb_home, shiftx2_home, pdb_id, chain_id, suffix, command_string):
    valid2 = run_shiftx2(pdb_home, shiftx2_home, pdb_id, chain_id, ".pdb.shiftX2", "python %s/shiftx2.py -i %s -c %s -o %s")       # ShiftX2
    validn = run_shiftx2(pdb_home, shiftx2_home, pdb_id, chain_id, ".pdb.shiftXN", "python %s/shiftx2.py --nmr -i %s -c %s -o %s") # ShiftX2 in NMR mode
    validp = run_shiftx2(pdb_home, shiftx2_home, pdb_id, chain_id, ".pdb.shiftXP", "python %s/shiftx2.py -n -i %s -c %s -o %s")    # exclude ShiftY ( = only ShiftXP)
    valid1 = run_shiftx2(pdb_home, shiftx2_home, pdb_id, chain_id, ".pdb.shiftX1", "python %s/shiftx2.py -n -x -i %s -c %s -o %s") # above plus fallback ShiftX1!

    # add ShiftX2
    tmp = createShiftX2OutfileName(pdb_id, chain_id, ".pdb.shiftX2")
    if (valid2 and os.path.exists(tmp)): 
      addValues(tmp, pdb_id, chain_id, "ShiftX2")
    # add ShiftX+  
    tmp = createShiftX2OutfileName(pdb_id, chain_id, ".pdb.shiftXP")
    if (validp and os.path.exists(tmp)): 
      addValues(tmp, pdb_id, chain_id, "ShiftXP") 
    # add ShiftX1  
    tmp = createShiftX2OutfileName(pdb_id, chain_id, ".pdb.shiftX1")
    if (valid1 and os.path.exists(tmp)): 
      addValues(tmp, pdb_id, chain_id, "ShiftX1")
    # add ShiftX2 in NMR mode   
    tmp = createShiftX2OutfileName(pdb_id, chain_id, ".pdb.shiftXN")
    if (validn and os.path.exists(tmp)): 
      addValues(tmp, pdb_id, chain_id, "ShiftXN")

    #exit(1)
    
  elif chain_id != "":
    print "WARNING: No ChainID given!"
  conn.commit()
