#!/usr/bin/Rscript
#
#
#		NightShift: NMR shift inference by General Hybrid model Training 
#		 --- A pipeline for automated NMR shift prediction training ---
#		
#		Anna Dehof
#

library(RSQLite)

# if you want to prevent that we use command line arguments,
# just run
#   nmr_noinit <- 1
# before sourcing
if (!exists("nmr_noinit")) {
	args <- commandArgs(TRUE)
} else { 	
	# TODO check if correct! 
  	args <- c("-training_db=/Users/andreas/src/NightShift/data/nmr_shift.db", "-dataset_indices=/Users/andreas/src/NightShift/data/results/Protein_RandomForestModel_TestAndTrainingIndices.txt")
}
verbose <- TRUE

source("NMR_functions.R")
setDefaultPipelineArgs()
handleCommandArgs(args)

createAtomTypes()

# connect to the DB
connectDB(training_db)

# and read the composition of the data set
dataset <- read.csv(dataset_indices, stringsAsFactors=FALSE)

# which models did we build for this dataset?
models = unique(dataset[,"model"])

print("Analyzing models for:")
print(models)

for (model in models) {
	print(paste("Analysis for ", model, ":", sep=""))
  
  current_data <- dataset[dataset$model==model,]

	current_train <- current_data[current_data$type=="TRAIN", "rowid"]
  current_test  <- current_data[current_data$type=="TEST",  "rowid"]

  print(paste("The model has been trained on ", nrow(current_data), " shifts with ", length(current_train), " used for training and ", length(current_test), " for testing.", sep=""))

  # get the shift data for this model
  data_shifts <- getShiftData(model, atom_superclasses, min_align_score, amber_cutoff)

  # and restrict it to those rows used for the model
  data_shifts <- data_shifts[data_shifts$rowid %in% current_data$rowid,]

  ## add flag for terminal residues
	data_shifts <- fixTerminalResidues(data_shifts)
	data_shifts <- fixBondLengths(data_shifts)

	data_shifts_train <- data_shifts[data_shifts$rowid %in% current_train,]
	data_shifts_test  <- data_shifts[data_shifts$rowid %in% current_test, ]

  ## count the secondary structure types
	num_coil_total <- length(which(data_shifts$SS == "C")) / nrow(data_shifts) * 100
	num_coil_train <- length(which(data_shifts_train$SS == "C")) / length(current_train) * 100
	num_coil_test <- length(which(data_shifts_test$SS == "C"))  / length(current_test) * 100

	num_helix_total <- length(which(data_shifts$SS == "H")) / nrow(data_shifts)  * 100
	num_helix_train <- length(which(data_shifts_train$SS == "H")) / length(current_train) * 100
	num_helix_test <- length(which(data_shifts_test$SS == "H")) / length(current_test) * 100

	num_sheet_total <- length(which(data_shifts$SS == "B")) / nrow(data_shifts) * 100
	num_sheet_train <- length(which(data_shifts_train$SS == "B")) / length(current_train) * 100
	num_sheet_test <- length(which(data_shifts_test$SS == "B")) / length(current_test) * 100

  print("Secondary structure composition in percent (Coil/Helix/Sheet):")
	print(paste("total: ", round(num_coil_total, 2), "/", round(num_helix_total, 2), "/", round(num_sheet_total, 2)))
	print(paste("train: ", round(num_coil_train, 2), "/", round(num_helix_train, 2), "/", round(num_sheet_train, 2)))
	print(paste("test: ",  round(num_coil_test,  2), "/", round(num_helix_test,  2), "/", round(num_sheet_test,  2)))
}
