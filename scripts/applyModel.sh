#!/bin/bash

#
#		NightShift: NMR shift inference by General Hybrid model Training 
#		 --- A pipeline for automated NMR shift prediction training ---
#		
#		Anna Dehof
#

# remember the directory from which we were called
olddir=$(pwd)

# change into our pipeline base directory
cd $(dirname $0)

source ./NMR_pipeline_definitions.sh

# we want to read the feature file from the command line 
feature_file=""

export PATH=$(readlink -f ../build):$PATH

# explain how to use the script
NO_ARGS=0 
E_OPTERROR=85
if [ $# -eq "$NO_ARGS" ]    # Script invoked with no command-line args?
then
	echo "Usage: `basename $0` options (--pdbfile=... --protchain=... --outfile=... [--Rmodel=... --features=...| (--predtype=[PureProtein|ProteinLigand])]  --outfile=... )" 
  exit $E_OPTERROR          # Exit and explain usage.
                            # Usage: scriptname -options
                            # Note: dash (-) necessary
fi

# parse the command line arguments
while getopts ":-:" Option; do
  case $Option in
		- )
			case "${OPTARG}" in
				pdbfile=*)
					export pdb_file=${OPTARG#*=}
				  ;;
				protchain=*)
					export prot_chain_id=${OPTARG#*=}
				  ;;
				outfile=*)
					export outfile=${OPTARG#*=}	
          ;;
				Rmodel=*)
					export Rmodel=${OPTARG#*=}	
          ;;
				features=*)
					export feature_file=${OPTARG#*=}	
          ;;
        predtype=*)
					export prediction_type=${OPTARG#*=}
				  ;;
			esac
  esac
done

# check command line arguments
if [ "${pdb_file}" = "" ]; then
  echo "Error: invalid PDB file!"
  exit $E_OPTERROR  
fi

if [ "${prediction_type}" = "" ]; then
  echo "Error: invalid prediction type!"
  exit $E_OPTERROR  
fi

if [ "${outfile}" = "" ]; then
  echo "Error: invalid outfile!"
  exit $E_OPTERROR  
fi


if [ "${Rmodel}" = "" ]; then	
	if [ "${feature_file}" = "" ]; then
		# translate $prediction_type into the correct prediction scenario! (-Rmodel=... -features...)
		if [ "${prediction_type}" = "PureProtein" ]; then	
			echo "The Pure Protein option!"
			#TODO use export??
			Rmodel=/home/HPL/anne/Desktop/Protein_RandomForestModel.Rdata # TODO point to our galaxy data directory?
			feature_file=/home/HPL/anne/Desktop/features.txt # TODO point to our galaxy data directory?
			#outfile="Spinster.csv"
			echo "   Use ${Rmodel}, ${feature_file}, ${outfile}"
		elif [ "${prediction_type}" = "ProteinLigand" ]; then
			#TODO Protein Ligand case
			echo "The Protein - Ligand option is currently not yet implemented. Sorry!"
			exit
		else	
			echo "Unknown prediction type ", ${prediction_type}
			exit $E_OPTERROR
		fi
	else
		echo "Either specify the predtype or give features __and__ RModel. Abort!"
		exit $E_OPTERROR  
	fi
fi

# for debugging
#echo "----------------------"
#echo "   applyModel.sh "
#echo "----------------------"
#echo "PDB: ",    ${pdb_file}
#echo "prot_chain: ",  ${prot_chain_id}
#echo "feature_file: ", ${feature_file} 
#echo "pred_type: ", ${prediction_type}
#echo "outfile: ", "${outfile}"  
#echo "log_file ", "$log_file}"
#echo "----------------------"

logfile_name=${logdir}applyModel_$$.log 

echo "----------------------------------------------------"
echo " ** Compute NMR descriptors..."
echo "----------------------------------------------------"
# TODO ../build unschoen! geht das besser?
echo "../build/ComputeNMRDescriptors  -pdb_file ${pdb_file} \
	-prot_chain ${prot_chain_id} \ 
 -feature_file ${feature_file} \ 
 -app_type prediction \ 
 -outfile prediction_$$.db \ 		
 -log_file prediction_$$.log"
../build/ComputeNMRDescriptors  -pdb_file ${pdb_file} \
                               	-prot_chain ${prot_chain_id} \
																-feature_file ${feature_file} \
												 			  -app_type prediction \
												 				-outfile prediction_$$.db \
															 	-log_file prediction_$$.log > ${logfile_name} 2>&1



echo "----------------------------------------------------"
echo " ** Query the model for predition..."
echo "----------------------------------------------------"
./applyModel.R -query_db=prediction_$$.db  -model_name=${Rmodel} -outfile=${outfile} >> ${logfile_name}  2>&1 
#echo "./applyModel.R -query_db=prediction_$$.db  -model_name=${Rmodel} -outfile=${outfile}"
rm prediction_$$.db

echo "----------------------------------------------------"
echo " ** Done. Find result in ${outfile}"
echo "          Find the logs in ${logfile_name}"
echo "----------------------------------------------------"

# go back
cd ${olddir}

echo " Done."
# done.
