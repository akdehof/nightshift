#!/bin/bash

# remember the directory from which we were called
olddir=$(pwd)

# change into our pipeline base directory
cd $(dirname $0)

source $(dirname $0)/NMR_pipeline_definitions.sh

export PATH=$(readlink -f ../build):$PATH

##########################################################

# explain how to use the script
NO_ARGS=0 
E_OPTERROR=85
if [ $# -eq "$NO_ARGS" ]    # Script invoked with no command-line args?
then
  echo "Usage: `basename $0` options (--data_db=... --eval_out=... --Rmodel=... --train_size=... --predtype=[RandomForest|Linear] --amber_cutoff=...)" 
  exit $E_OPTERROR          # Exit and explain usage.
                            # Usage: scriptname -options
                            # Note: dash (-) necessary
fi

# parse the command line arguments
while getopts ":-:" Option; do
  case $Option in
		- )
			case "${OPTARG}" in
				data_db=*)
					export nmr_sqlite_db=${OPTARG#*=}
				  ;;
				eval_out=*)
					export eval_file=${OPTARG#*=}
				  ;;
			  predtype=*)
					export model_type=${OPTARG#*=}
				  ;; 
				Rmodel=*)
					export Rmodel=${OPTARG#*=}
				  ;;
				train_size=*)
					export train_size=${OPTARG#*=}
				  ;;
				amber_cutoff=*)
					export amber_cutoff=${OPTARG#*=}
					;;
			esac
  esac
done

# for debugging
#echo "nmr_sqlite_db", ${nmr_sqlite_db}
#echo "Rmodel", ${Rmodel}
#echo "train_size", ${train_size}
#echo "model_type", ${model_type}
#echo "eval_file", ${eval_file}
#echo "train_size", ${train_size}

##########################################################

if [ "${model_type}" = "Linear" ]; then 
	#TODO user adaption as for RF
  echo "************************************************************************"
  echo "  Training a linear model... (this may take a while...)"
	echo "      (see ${logdir}train_linear_model.log)"  	
	echo "  The resulting model will be placed in:" 
	echo "      ${Rmodel}" 
	echo "  The evaluation will be placed in:"  
	echo "      ${eval_file}"
  echo "************************************************************************"
	#TODO commands anpassen!!! -d=...
	./create_models_modular.R ${nmr_sqlite_db} -outfile={$Rmodel} > ${logdir}train_linear_model.log 2>&1
	python extract_results_for_tex.py ${logdir}train_linear_model.log > ${resultsdir}evaluation_linear_model.tex 2> /dev/null
	cd ${resultsdir}
	pdflatex evaluation_linear_model.tex > /dev/null 2>&1
	cd -
fi

if [ "${model_type}" = "RandomForest" ]; then
  echo "************************************************************************"
  echo "  Training a random forest model... (this may take a while...)"
	echo "      (see ${logdir}$$_train_rf_model.log)" 
 	echo "  The resulting model will be placed in:" 
	echo "      ${Rmodel}" 
	echo "  The evaluation will be placed in:"
  echo "      ${eval_file}"
  echo "************************************************************************"
	#	Rmodel="${resultsdir}/Protein_RandomForestModel.Rdata" 
	#TODO: -train_size=${train_size} 
	./create_models_modular.R -training_db=${nmr_sqlite_db} -use_random_forest=true -model_name=${Rmodel} -train_size=${train_size} -amber_cutoff=${amber_cutoff} > ${logdir}$$_train_rf_model.log 2>&1
	python extract_results_for_tex.py ${logdir}$$_train_rf_model.log > ${resultsdir}$$_evaluation_rf_model.tex 2> /dev/null
	cd ${resultsdir}
	pdflatex $$_evaluation_rf_model.tex > /dev/null 2>&1
	mv $$_evaluation_rf_model.pdf ${eval_file}
	cd -
	#echo "DEBUG trainNMRModel.sh: results are: ${Rmodel}, ${resultsdir}/$$_evaluation_rf_model.tex, ${eval_file}"
fi

# go back
cd ${olddir}

echo " Done."
# done.
