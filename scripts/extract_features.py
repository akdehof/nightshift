#!/usr/bin/python
import os
import sys 
import re


#
#    the feature names
#  --- first all ---
feature_names = {
 'PDB_ID'        : "",
 'CHAIN_ID'      : "", 
 'RES_ID'        : "", 
 'ATOM_NAME'     : "atom_name", 
 'EXP_SH'        : "", 
 'COMP_SH'       : "", 
 'RING_CURR'     : "ring_current", 
 'ELEC_FIELD'    : "electric_field",
 'RANDOM_COIL'   : "random_coil",
 'HBOND_EFF'     : "hbond_effect",
 'EHS'           : "",
 'ELEMENT'       : "element",
 'pH'            : "pH",
 '"TEMP"'        : "temperature", ## ""??
 'PRESS'         : "press",
 'H_NMR_SOLUTION': "",
 'C_NMR_SOLUTION': "",
 'N_NMR_SOLUTION': "",
 'AA'            : "aa",
 'AAnext'        : "aa_next",
 'AAprev'        : "aa_prev",
 'AMBERALL'      : "Amber",
 'AMBERBEND'     : "AmberBend",
 'AMBERES'       : "AmberES",
 'AMBERSTRETCH'  : "AmberStretch",
 'AMBERTORSION'  : "AmberTorsion",
 'AMBERVDW'      : "AmberVDW",
 'ATODEN'        : "atom_density",
 'ATOMSA'        : "atom_sas",
 'ATOMSASR'      : "atom_sas2",
 'ATOPAC'        : "atom_pack",
 'ATOWEIDEN'     : "atom_w_density",
 'ATOWEIPAC'     : "atom_w_pack",
 'CHARGE'        : "charge",
 'CHI'           : "$\\chi$", ##
 'CHI2'          : "$\\chi$2",
 'CHI2next'      : "$\\chi$2_next",
 'CHI2prev'      : "$\\chi$2_prev",
 'CHInext'       : "$\\chi$_next",
 'CHIprev'       : "$\\chi$_prev",
 'COM'           : "dist_com",
 'DISTCA'        : "dist_CA",
 'DISTCB'        : "dist_CB",
 'DISTN'         : "dist_N",
 'DISTO'         : "dist_O",
 'DISULPHIDE'    : "disulfide",
 'DISULPHIDEL'   : "disulfide_len",
 'DISULPHIDETA'  : "disulfide_ang",
 'FR'            : "first_residue",
 'HA'            : "hbond_HA",
 'HA2'           : "hbond_HA2",
 'HA2A'          : "hbond_HA2_ang",
 'HA2L'          : "hbond_HA2_len",
 'HAA'           : "hbond_HA_ang",
 'HAL'           : "hbond_HA_len",
 'HBOND'         : "hbond",
 'HBONDA'        : "hbond_ang",
 'HBONDDONOR'    : "hbond_donor",
 'HBONDDONORA'   : "hbond_donor_len",
 'HBONDDONORL'   : "hbond_donor_ang",
 'HBONDL'        : "hond_len",
 'HETATOM'       : "", ##??
 'HETRES'        : "", ##??
 'HN'            : "hbond_HN",
 'HNA'           : "hbond_HN_ang",
 'HNL'           : "hbond_HN_len",
 'OH'            : "hond_OH",
 'OHA'           : "hbond_OH_ang",
 'OHL'           : "hbond_OH_len",
 'PHI'           : "$\\phi$",
 'PHInext'       : "$\\phi$_next",
 'PHIprev'       : "$\\phi$_prev",
 'PSI'           : "$\\psi$",
 'PSInext'       : "$\\psi$_next",
 'PSIprev'       : "$\\psi$_prev",
 'RESIDUESA'     : "residue_sas",
 'RESIDUESASR'   : "residue_sas2",
 'ROTAMER'       : "rotamer",
 'ROTAMERDIST'   : "rotamer_dist", 
 'ROTAMERINDEX_' : "residue_rotamer",
 'SS'            : "secondary_structure",
 'TempFac'       : "",
 'ShiftX2'       : "",
 'ShiftX1'       : "",
 'ShiftXP'       : "",
 'RefDB'         : "",
 'ALIGN_SCORE'   : "",  
 'Alignment_PDB' : "", 
 'Alignment_BMRB': "", 
 'PDB_R_FACT'    : "", 
 'PDB_YEAR'      : "", 
 'NMR_YEAR'      : "", 
 'PROTEIN_SIZE'  : "protein_size", 
 'HAS_H_SHIFTS'  : "has_H_shifts", 
 'HAS_C_SHIFTS'  : "has_C_shifts", 
 'HAS_N_SHIFTS'  : "has_N_shifts", 
 'IS_NMR'        : "", 
 'HAS_ION'       : "has_ion", 
 'HAS_LIGAND'    : "has_ligand", 
 'HAS_DNA'       : "has_DNA",  
 'LIGAND_SIZE'   : "",  
 'IONS'          : "", 
 'NMR_SPECT'     : "spectrometer", 
 'NMR_SPECT_MANUFAC' : "manufacturer",
 'NMR_FIELD_STRENGTH': "field_strength",
 ### ligand stuff
 'CL_HET_DIST'       : "cl_het_dist",
 'CL_HET_ELEMENT'    : "cl_het_element",
 'CL_HET_GAFFType'   : "cl_gaff_type",
 'NUM_HET_ATOMS'     : "num_het_atoms",
 'LIGAND_SIZE'       : "ligand_size",
 'GAFFTYPE_'         : "gaff_type",
 'GAFFTYPE_X_DIST'   : "gaff_type_X_dist"
}

#
#    the feature names
#  --- then mark our new features as bold ---
for i in [ 'H_NMR_SOLUTION'
,'C_NMR_SOLUTION'
,'N_NMR_SOLUTION'
,'AMBERALL' 
,'AMBERBEND'   
,'AMBERES'     
,'AMBERSTRETCH'
,'AMBERTORSION'
,'AMBERVDW'    
,'ATODEN'      
,'ATOMSA'      
,'ATOMSASR'    
,'ATOPAC'      
,'ATOWEIDEN'  
,'ATOWEIPAC'   
, 'CHARGE'       
, 'COM'          
#, 'DISTCA'     
#, 'DISTCB'     
#, 'DISTN'      
#, 'DISTO'      
, 'DISULPHIDEL'  
, 'DISULPHIDETA' 
, 'HBOND'        
, 'HBONDA'       
, 'HBONDDONOR'   
, 'HBONDDONORA'  
, 'HBONDDONORL'  
, 'HBONDL'        
, 'HETATOM'       
, 'HETRES'        
, 'RESIDUESA'     
, 'RESIDUESASR'   
, 'ROTAMER'       
, 'ROTAMERDIST'   
, 'ROTAMERINDEX_'   
, 'TempFac'       
# 'ALIGN_SCORE'  
# 'PDB_R_FACT'   
# 'PDB_YEAR'     
# 'NMR_YEAR'     
, 'PROTEIN_SIZE'   
, 'HAS_H_SHIFTS'   
, 'HAS_C_SHIFTS'   
, 'HAS_N_SHIFTS'   
, 'IS_NMR'        
, 'HAS_ION'       
, 'HAS_LIGAND'    
, 'HAS_DNA'       
, 'LIGAND_SIZE'   
, 'IONS'          
, 'NMR_SPECT'      
, 'NMR_SPECT_MANUFAC' 
, 'NMR_FIELD_STRENGTH'
 ### ligand stuff
, 'CL_HET_DIST'       
, 'CL_HET_ELEMENT'    
, 'CL_HET_GAFFType'   
, 'NUM_HET_ATOMS'     
, 'LIGAND_SIZE'       
]:
  feature_names[i] = "{\\bf " + feature_names[i] + " } "  


#############################################
##
##           MAIN
##
#############################################

if (len(sys.argv) < 2):
  print "Usage: %s train_rf_model.log [significance_cutoff [new_feature_file]] " % sys.argv[0]
  exit (1)
  
#[1] "** building protein model for  N "
#[1] "print rounded importance"
#             %IncMSE IncNodePurity
#RING_CURR      42.80       6135.01
#ELEC_FIELD      0.00          0.00
#HBOND_EFF       0.00          0.00
#TEMP           28.04       2787.79
#AMBERALL       28.19       2863.52
#AMBERBEND       0.00          0.00
#AMBERES        28.11       2878.89
#AMBERSTRETCH   35.54       4076.01
#AMBERTORSION   40.97       6806.85
#SS             22.29       1720.12
#[1] "Evaluate the model:"
#[1] "size train for N : 20749"
#[1] "loaded data size: 73276" "loaded data size: 451" 
#[1] "training set contains  20749 lines."
#[1] "test set contains  13834 lines."
#'data.frame':	20749 obs. of  47 variables:


atom_pattern    = re.compile("\[1\] \"\\*\\* building [proteinligand]+ model for\s+([A-Za-z1234]+)\s+\"")
feature_pattern = re.compile("([A-Za-z][A-Za-z_0-9]+)\s+([0-9.]+)\s+([0-9.]+)")

ori_size_pattern   = re.compile("\[1\]\s+\"initial data size\s+([0-9]+)\"\s+\"initial data size\s+([0-9]+)\"") 
train_size_pattern = re.compile("\[1\]\s+\"size train for ([A-Za-z1234]+)\s+:\s+([0-9]+)\"")
test_size_pattern  = re.compile("\[1\]\s+\"size test\s+for\s+([A-Za-z1234]+)\s+:\s+([0-9]+)\"")
prov_num_feature_pattern =  re.compile("'data.frame':\s+([0-9]+)\s+obs.\s+of\s+([0-9]+)\s+variables:")
                                       #'data.frame':	44643 obs. of  64 variables:

features = {}
atoms = []
num_models = 0

# per atom
features_per_atom = {}
num_sel_features = {}
num_ori_shifts   = {}
num_ori_features = {}
size_train_set = {}
size_test_set  = {}
num_gaff_features = {}
num_rotamer_features = {}
prov_features = {}

significant_features = {}

is_ligand_set = False
feature_sig_cutoff = 0.0
if (len(sys.argv) > 2):
  feature_sig_cutoff = float(sys.argv[2])

lines = open(sys.argv[1], 'r').readlines()
for line in lines:
  feature_result  = re.match(feature_pattern, line) 
  atom_result     = re.match(atom_pattern, line)  
  ori_size_result = re.match(ori_size_pattern, line)
  train_size_result = re.match(train_size_pattern, line)
  test_size_result  = re.match(test_size_pattern, line)
  prov_num_feature_result = re.match(prov_num_feature_pattern, line) 
  # do we have a new atom?
  if (atom_result):
    #print "\n\n **", atom_result.group(1)
    atom = atom_result.group(1)
    num_models += 1
    atoms.append(atom) 
  # do we have a feature line?
  if (feature_result):
    #print feature_result.groups()
    feature = feature_result.group(1)
    importance = feature_result.group(2)
    # store the feature
    if not features.has_key(feature):
      features[feature] = 1
    # store the feature and its importance
    if not features_per_atom.has_key(atom):
      features_per_atom[atom]={}
    if features_per_atom.has_key(atom):
      features_per_atom[atom][feature]=importance
      # is it a gaff or a rotamer feature?
      # count binarized features
      if (not num_gaff_features.has_key(atom)):
        num_gaff_features[atom] = 0
      if ((feature[:len("GAFFTYPE_")] == "GAFFTYPE_" ) and (num_gaff_features.has_key(atom))):
        is_ligand_set = True
        num_gaff_features[atom] += 1  
      if (not num_rotamer_features.has_key(atom)):
        num_rotamer_features[atom] = 0
      if ((feature[:len("ROTAMERINDEX_")] == "ROTAMERINDEX_") and (num_rotamer_features.has_key(atom))):
        num_rotamer_features[atom] += 1
        #print "huhu"
    # count the number of features
    if (importance > feature_sig_cutoff):
      if (feature.find("ROTAMERINDEX_")==0):
        significant_features["ROTAMERINDEX_"]=1 
      elif (feature.find("GAFFTYPE_")==0):
        significant_features["GAFFTYPE_"]=1 
      else:
        significant_features[feature]=1 
      #print atom, feature
      if (not num_sel_features.has_key(atom)):
        num_sel_features[atom]=1
      else:
        num_sel_features[atom]+=1
  # do we have a ori size line
  if (ori_size_result):
    #print ori_size_result.groups()
    num_ori_shifts[atom]   = ori_size_result.group(1)
    num_ori_features[atom] = ori_size_result.group(2)
  # do we have a train size line?
  if (train_size_result):
    size_train_set[train_size_result.group(1)] = train_size_result.group(2)
  # do we have a test size line?
  if (test_size_result):
    size_test_set[test_size_result.group(1)] = test_size_result.group(2)
  if (prov_num_feature_result):
    prov_features[atom] = prov_num_feature_result.group(2)


#
#  determine the top 10 features per atom

ligand_cutoff = 20

feature_importance_cutoff_per_atom = {}
for atom in atoms:
  val = []
  for feature in features:
    if features_per_atom.has_key(atom) and features_per_atom[atom].has_key(feature) and float(features_per_atom[atom][feature])>0.:
      val.append(float(features_per_atom[atom][feature]))
  # sort in ascending order
  val.sort()
  #print atom, val[len(val)-10]
  if (len(val) >=10):
    feature_importance_cutoff_per_atom[atom] = val[len(val)-10]
  elif (len(val) > 0):
    feature_importance_cutoff_per_atom[atom] = val[-1]


# sort the features!
sorted_features = []
for feature in features:
  sorted_features.append(feature)
sorted_features.sort()

#print sorted_features

#####################################################
##
##             Feature importance per atom type 
##
#####################################################

doc_header = """\documentclass{article}
\usepackage[landscape]{geometry}
\\begin{document}"""

tab_header = """\\begin{table}[htbp]
  \\begin{center}
   \\tiny
   \\begin{tabular}{%s}
  \hline """ %("|l|" + ("c|" * len(atoms)))

print doc_header 
print tab_header

# the features and their importance
#print "\n--------------------\n"
print "feature",
for atom in atoms:
  print "&", atom, 
print "\\\\"
print "\hline"

#for feature in features:
for feature in sorted_features:
  important_feature = False
  for atom in atoms:
    if features_per_atom.has_key(atom) and features_per_atom[atom].has_key(feature) and float(features_per_atom[atom][feature])>0.:
      important_feature = True
  if important_feature:
    # beautify the feature name
    tex_feature = feature
    if (feature_names.has_key(feature)): 
      tex_feature = feature_names[tex_feature]
    tex_feature = tex_feature.replace("_", "\\_")
    print tex_feature,
    # now print the importance values
    for atom in atoms:
      if   features_per_atom.has_key(atom) and features_per_atom[atom].has_key(feature):
        # print the most important ligand features bold
        if (float(features_per_atom[atom][feature]) >=  float(feature_importance_cutoff_per_atom[atom])):
        #if (float(features_per_atom[atom][feature]) > float(ligand_cutoff)):
          print  "& {\\bf ", features_per_atom[atom][feature], " } ",  
        else:
          print  "&", features_per_atom[atom][feature], 
      else:
        print "& 0.00 ", 
    print "\\\\"
print "\hline"

# the number of selected features
print "num significant features",
for atom in atoms:
  if num_sel_features.has_key(atom):
    print "&", int(num_sel_features[atom])/2,
  else:
    print "&", "  ",
print "\\\\"

print """\hline
\end{tabular}
\end{center}
\caption{Importance of the features by atom super class.}
\end{table}"""
#\end{document}"""

#####################################################
##
##             Feature number summary
##
#####################################################
#print "\n--------------------\n"

print("""\\begin{table}[htbp]
  \\begin{center}
   \\tiny
   \\begin{tabular}{%s}
   \hline
      """ %("|l|" + ("c|" * len(atoms))))

print "  ", 
for atom in atoms:
 print  "&", atom,
print "\\\\\hline \hline "

# the original number of shifts
print "initial num shifts", 
for atom in atoms:
  if num_ori_shifts.has_key(atom):
    print "&",  num_ori_shifts[atom],       
  else:
    print "&",  "  ",
print "\\\\"

# the original number of features 
print "initial num features (incl. logistic requirements)", 
for atom in atoms:
  if num_ori_features.has_key(atom):
    print "&",  num_ori_features[atom], 
  else:
    print "&", "  ",
print "\\\\"

# the final trainings size 
print "final data size train", 
for atom in atoms:
  if size_train_set.has_key(atom):
    print "&",   size_train_set[atom], 
  else:
    print "&", "  ",
print "\\\\"


# the final test size 
print "final data size test", 
for atom in atoms:
  if size_test_set.has_key(atom):
    print "&",  size_test_set[atom], 
  else:
    print "&", "  ",
print "\\\\"

# the number of selected features
print "provided features",
for atom in atoms:
  if prov_features.has_key(atom):
    print "&", int(prov_features[atom]),
  else:
    print "&", "  ",
print "\\\\"

# the number of selected features
print "significant features",
for atom in atoms:
  if num_sel_features.has_key(atom):
    print "&", int(num_sel_features[atom])/2,
  else:
    print "&", "  ",
print "\\\\"

## the number of gaff features
#print "num gaff features",
#for atom in atoms:
#  if num_gaff_features.has_key(atom):
#    print "&", int(num_gaff_features[atom])/2,
#  else:
#    print "&", "  ",
#print "\\\\"

#
## the number of rotamer features
#print "num rotamer features",
#for atom in atoms:
#  if num_rotamer_features.has_key(atom):
#    print "&", int(num_rotamer_features[atom])/2,
#  else:
#    print "&", "  ",
#print "\\\\"

## the number of real features
#print "num \"real\" features",
#for atom in atoms:
#  if num_rotamer_features.has_key(atom):
#    # the ligand case:
#    #if num_gaff_features.has_key(atom):
#    if is_ligand_set:
#      print "&", int(num_sel_features[atom])/2 - int(num_gaff_features[atom])/2 ,
#    else:
#      print "&", int(num_sel_features[atom])/2 - int(num_rotamer_features[atom])/2 ,
#  else:
#    print "&", "  ",
#print "\\\\"

print """\hline
\end{tabular}
\end{center}
\caption{Summary of the features.}
\end{table}"""

print """\end{document}"""

# now print the significant features
if (len(sys.argv) > 3):
  outfile = open(sys.argv[3], 'w')
  outfile.write("##\n## This file is automatically generated to contain all significant\n## features of a previous run.\n##\n")
  for feature in significant_features:
    outfile.write(feature + " : bla\n")
  outfile.close()



