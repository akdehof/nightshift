ballpath NMR 

########
## some exports
export prediction_type="PureProtein" # can be PureProtein or ProteinLigand

export datadir="/usr/local/NMR/pipeline/nmr-data/"
export resultsdir="${datadir}/results/"
export logdir="${datadir}/logs/"

## path to the pisces binary, if available, or empty
export pisces_path="/usr/local/PISCES/"
export pisces_maxpc="10"
export pisces_parameters="-r 0.0-3.0 -l 40-10000 -f 1 -x T -a T"

## path to the ShiftX2 installation, if available, or empty
export shiftx2_path=""

export mappingfilename="$datadir/mapping_PROTEIN.txt"               # protein
export mappingfilename_ligand="$datadir/mapping_LIGAND.txt"         # protein-ligand
export mappingfilename_dna="$datadir/mapping_DNA.txt"               # dna
export mappingfilename_dna_ligand="$datadir/mapping_DNA_LIGAND.txt" # dna-ligand

export pdbhome="$datadir/PDB_PROTEIN/"                                                     # protein
export pdbhome_ligand="$datadir/PDB_LIGAND/"                                               # protein-ligand
export pdbhome_dna="$datadir/PDB_DNA/"                                                     # dna
export pdbhome_dna_ligand="$datadir/PDB_DNA_LIGAND/"                                       # dna-ligand
export pdbhome_shiftx2_set="$datadir/shiftx2-testset-June2011/PDB-testset-addHydrogens/"   # ShiftX2 test set

export bmrbhome="$datadir/BMRB_PROTEIN/"                # protein
export bmrbhome_ligand="$datadir/BMRB_LIGAND/"          # ligand
export bmrbhome_dna="$datadir/BMRB_DNA/"                # dna
export bmrbhome_dna_ligand="$datadir/BMRB_DNA_LIGAND/"  # dna-ligand
export bmrbhome_refdb="$datadir/BMRB_REFDB/"            # reference corrected

export nmr_sqlite_db="$datadir/nmr_shift.db"
export num_filldb_procs=4 # number of parallel processes to compute atom properties
export pisces_binary="${pisces_path}bin/Cull_for_UserPDB.pl"
export source_dir=$(pwd)

